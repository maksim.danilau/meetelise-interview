package com.meetelise.maksim.danilau.interview.task01.service;

import com.meetelise.maksim.danilau.interview.task01.config.Beans;
import com.meetelise.maksim.danilau.interview.task01.model.Property;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class PropertyAvailabilityServiceTest {

    private static Property property;

    @BeforeAll
    public static void init() throws Exception {
        URL url = new URL("https://interviewexample.s3-us-west-2.amazonaws.com/interview.json");
        property = Beans.SCHEDULE_READER.readValue(url);
    }

    @Test
    public void test() {
        ZonedDateTime dateTime = ZonedDateTime.of(LocalDate.now(), LocalTime.of(12, 0, 0), ZoneId.of("America/New_York"));
        boolean result = PropertyAvailabilityService.INSTANCE.isPropertyAvailable(dateTime, property);
        Assertions.assertTrue(result);
    }
}
