package com.meetelise.maksim.danilau.interview.task02;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class AlgorithmTest {

    ProfitMaximizerService service = new ProfitMaximizerService();

    @Test
    public void test_1_3() {
        List<Integer> prices = Arrays.asList(1,2,3,4);
        List<Integer> desiredResult = Arrays.asList(0, 3, 3);
        List<Integer> result = service.determineMaxProfitDays(prices);

        Assertions.assertEquals(result, desiredResult);
    }


    @Test
    public void test_1_2() {
        List<Integer> prices = Arrays.asList(4,3,2,1);
        List<Integer> desiredResult = Arrays.asList(2, 3, -1);
        List<Integer> result = service.determineMaxProfitDays(prices);

        Assertions.assertEquals(result, desiredResult);
    }


    @Test
    public void test_1_7() {
        List<Integer> prices = Arrays.asList(99, 100, 1, 5);
        List<Integer> desiredResult = Arrays.asList(2, 3, 4);
        List<Integer> result = service.determineMaxProfitDays(prices);

        Assertions.assertEquals(result, desiredResult);
    }

}
