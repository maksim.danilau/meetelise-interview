package com.meetelise.maksim.danilau.interview.task01.model;

import java.io.Serializable;

public class PropertyCalendarSettings implements Serializable {

    private Integer propertyId;
    private Object appointmentLength;
    private Availabilities availabilities;

    public Integer getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Integer propertyId) {
        this.propertyId = propertyId;
    }

    public Object getAppointmentLength() {
        return appointmentLength;
    }

    public void setAppointmentLength(Object appointmentLength) {
        this.appointmentLength = appointmentLength;
    }

    public Availabilities getAvailabilities() {
        return availabilities;
    }

    public void setAvailabilities(Availabilities availabilities) {
        this.availabilities = availabilities;
    }
}
