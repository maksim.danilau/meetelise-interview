package com.meetelise.maksim.danilau.interview.task01.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.DateDeserializers;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.time.LocalTime;
import java.time.OffsetTime;
import java.time.ZoneOffset;
import java.util.Date;

public class CustomOffsetTimeDeserializer extends StdDeserializer<LocalTime> {

    public static final CustomOffsetTimeDeserializer INSTANCE = new CustomOffsetTimeDeserializer();

    private CustomOffsetTimeDeserializer() {
        super(OffsetTime.class);
    }

    @Override
    public LocalTime deserialize(JsonParser parser, DeserializationContext context) throws IOException {
        Date date = DateDeserializers.DateDeserializer.instance.deserialize(parser, context);
        return OffsetTime.ofInstant(date.toInstant(), ZoneOffset.UTC).toLocalTime();
    }
}
