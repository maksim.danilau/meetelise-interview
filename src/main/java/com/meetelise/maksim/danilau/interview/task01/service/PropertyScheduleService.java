package com.meetelise.maksim.danilau.interview.task01.service;

import com.meetelise.maksim.danilau.interview.task01.config.Beans;
import com.meetelise.maksim.danilau.interview.task01.model.Property;

import java.io.IOException;
import java.net.URL;

public class PropertyScheduleService {

    public Property loadSchedule(URL scheduleURL) throws IOException {
        return Beans.SCHEDULE_READER.readValue(scheduleURL);
    }
}
