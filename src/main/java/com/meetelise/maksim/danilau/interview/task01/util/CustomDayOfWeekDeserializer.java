package com.meetelise.maksim.danilau.interview.task01.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.time.DayOfWeek;

public class CustomDayOfWeekDeserializer extends StdDeserializer<DayOfWeek> {

    public static final CustomDayOfWeekDeserializer INSTANCE = new CustomDayOfWeekDeserializer();

    private CustomDayOfWeekDeserializer() {
        super(DayOfWeek.class);
    }

    @Override
    public DayOfWeek deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        String name = p.getValueAsString().toUpperCase();
        return DayOfWeek.valueOf(name);
    }
}
