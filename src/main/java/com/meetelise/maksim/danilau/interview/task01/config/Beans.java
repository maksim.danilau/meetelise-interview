package com.meetelise.maksim.danilau.interview.task01.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.meetelise.maksim.danilau.interview.task01.model.Property;
import com.meetelise.maksim.danilau.interview.task01.util.CustomDayOfWeekDeserializer;
import com.meetelise.maksim.danilau.interview.task01.util.CustomOffsetTimeDeserializer;

import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalTime;

public class Beans {

    public static final ObjectMapper OBJECT_MAPPER = JsonMapper
            .builder()
            .defaultDateFormat(new SimpleDateFormat("hh:mm:ssz"))
            .addModule(
                    new SimpleModule()
                            .addDeserializer(LocalTime.class, CustomOffsetTimeDeserializer.INSTANCE)
                            .addDeserializer(DayOfWeek.class, CustomDayOfWeekDeserializer.INSTANCE))
            .build();

    public static final ObjectReader SCHEDULE_READER = OBJECT_MAPPER.readerFor(Property.class);
}
