package com.meetelise.maksim.danilau.interview.task01.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.time.DayOfWeek;
import java.time.LocalTime;

public class Availability implements Serializable {

    @JsonProperty("day")
    private DayOfWeek dayOfWeek;
    private LocalTime startTime;
    private LocalTime endTime;
    private Integer simultaneousAppointmentLimit;

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(DayOfWeek dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public Integer getSimultaneousAppointmentLimit() {
        return simultaneousAppointmentLimit;
    }

    public void setSimultaneousAppointmentLimit(Integer simultaneousAppointmentLimit) {
        this.simultaneousAppointmentLimit = simultaneousAppointmentLimit;
    }
}
