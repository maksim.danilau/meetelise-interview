package com.meetelise.maksim.danilau.interview.task01.service;

import com.meetelise.maksim.danilau.interview.task01.model.Availability;
import com.meetelise.maksim.danilau.interview.task01.model.Property;

import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;

public class PropertyAvailabilityService {

    public static final PropertyAvailabilityService INSTANCE = new PropertyAvailabilityService();

    private PropertyAvailabilityService() {
    }

    public boolean isPropertyAvailable(ZonedDateTime dateTime, Property property) {
        List<Availability> availabilities = property.getPropertyCalendarSettings().getAvailabilities().getAvailabilities();

        return availabilities.stream()
                .filter(availability -> availability.getDayOfWeek().equals(dateTime.getDayOfWeek()))
                .allMatch(availability -> {
                    LocalTime startTime = availability.getStartTime();
                    LocalTime endTime = availability.getEndTime();

                    ZonedDateTime utcTime = dateTime.withZoneSameInstant(ZoneOffset.UTC);
                    ZonedDateTime startDateTime = ZonedDateTime.of(
                            dateTime.toLocalDate(),
                            availability.getStartTime(),
                            ZoneOffset.UTC
                    );
                    ZonedDateTime endDateTime = ZonedDateTime.of(
                            dateTime.toLocalDate(),
                            availability.getEndTime(),
                            ZoneOffset.UTC
                    );
                    if (endTime.isBefore(startTime)) {
                        endDateTime = endDateTime.plusDays(1);
                    }


                    return startDateTime.compareTo(utcTime) <= 0 && endDateTime.compareTo(utcTime) >= 0;
                });
    }
}
