package com.meetelise.maksim.danilau.interview.task01.model;

import java.io.Serializable;

public class Property implements Serializable {

    private PropertyCalendarSettings propertyCalendarSettings;

    public PropertyCalendarSettings getPropertyCalendarSettings() {
        return propertyCalendarSettings;
    }

    public void setPropertyCalendarSettings(PropertyCalendarSettings propertyCalendarSettings) {
        this.propertyCalendarSettings = propertyCalendarSettings;
    }
}
