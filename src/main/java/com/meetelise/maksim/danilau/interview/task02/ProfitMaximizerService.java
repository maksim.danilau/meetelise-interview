package com.meetelise.maksim.danilau.interview.task02;

import java.util.ArrayList;
import java.util.List;

public class ProfitMaximizerService {

    public List<Integer> determineMaxProfitDays(List<Integer> prices) {
        Integer[] pricesArray = (Integer[]) prices.toArray();
        int[] resultArray = findBuySellPair(pricesArray, 0);

        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < resultArray.length; i++) {
            result.add(resultArray[i]);
        }

        return result;

    }

    private int[] findBuySellPair(Integer[] prices, int startPoint) {
        if (startPoint > prices.length - 2) {
            return new int[]{-1, -1, Integer.MIN_VALUE};
        }

        int buyingDay = startPoint;
        int minPrice = prices[buyingDay];

        int sellingDay = startPoint + 1;
        int maxPrice = prices[sellingDay];

        int profit = maxPrice - minPrice;

        if (startPoint + 2 < prices.length) {
            for (int dayNumber = startPoint + 2; dayNumber < prices.length; dayNumber++) {
                int currentPrice = prices[dayNumber];

                if (currentPrice < minPrice) {
                    if (dayNumber > sellingDay) {
                        int[] adjustedResult = findBuySellPair(prices, dayNumber);
                        int currentBuyingDay = adjustedResult[0];
                        int currentSellingDay = adjustedResult[1];
                        int currentProfit = adjustedResult[2];

                        if (currentProfit > profit) {
                            buyingDay = currentBuyingDay;
                            minPrice = prices[currentBuyingDay];

                            sellingDay = currentSellingDay;
                            maxPrice = prices[currentSellingDay];

                            profit = currentProfit;
                        }
                    }
                } else if (currentPrice > maxPrice) {
                    sellingDay = dayNumber;
                    maxPrice = currentPrice;

                    profit = maxPrice - minPrice;
                }
            }
        }

        return new int[]{buyingDay, sellingDay, profit};
    }

}
